from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Category(models.Model):
    name = models.CharField(max_length=50)
    state = models.BooleanField(default=True)


class Blog(models.Model):
    title = models.CharField(max_length=50)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    dateCreated = models.DateField(default=timezone.now)
    content = models.CharField(max_length=10000)
    likes = models.IntegerField()
    views = models.IntegerField()
    state = models.BooleanField(default=True)


class CategoryBlogs(models.Model):
    category = models.ForeignKey('Category')
    blog = models.ForeignKey('Blog')


class Commentary(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    content = models.TextField()
    state = models.BooleanField(default=True)
    blog = models.ForeignKey('Blog')
