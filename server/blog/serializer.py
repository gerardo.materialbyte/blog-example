from rest_framework.serializers import ModelSerializer
from .models import *
from django.db.models import Q


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password')


# Blog #
class BlogSerializer(ModelSerializer):
    user = UserSerializer(read_only=False)

    class Meta:
        model = Blog
        fields = ('id', 'title', 'user', 'dateCreated', 'content', 'likes', 'views', 'state')


class BlogSerializerPost(ModelSerializer):
    class Meta:
        model = Blog
        fields = '__all__'


# Commentary #
class CommentarySerializer(ModelSerializer):
    class Meta:
        model = Commentary
        fields = '__all__'
