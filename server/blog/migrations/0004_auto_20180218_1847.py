# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-18 22:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20180218_1846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='content',
            field=models.CharField(max_length=10000),
        ),
    ]
