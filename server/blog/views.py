from __future__ import unicode_literals
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from .serializer import *
from .models import *
from django.shortcuts import render


# User #
class GetAllUser(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Category #
class GetAllCategory(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


# Blogs #
class GetAllBlogs(ListAPIView):
    queryset = Blog.objects.all().select_related()
    serializer_class = BlogSerializer


class CreateBlog(CreateAPIView):
    queryset = Blog.objects.all().select_related()
    serializer_class = BlogSerializerPost


class GetAllCommentary(ListAPIView):
    queryset = Commentary.objects.all()
    serializer_class = CommentarySerializer
