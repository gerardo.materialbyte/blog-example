from django.conf.urls import  include, url
# from .views import ()
from rest_framework_swagger.views import get_swagger_view
from .views import *

urlpatterns = [
    url(r'^apidocs/$', get_swagger_view(), name='apidocs'),

    # Category #
    url(r'^category/all$', GetAllCategory.as_view(), name='AllCategory'),

    # Commentary #
    url(r'^commentary/all$', GetAllCommentary.as_view(), name='AllCommentary'),

    # User #
    url(r'^user/all$', GetAllUser.as_view(), name='AllUser'),

    # Blogs #
    url(r'^blogs/all$', GetAllBlogs.as_view(), name='AllBlogs'),
    url(r'^blogs/$', CreateBlog.as_view(), name='CreateBlog'),
]
